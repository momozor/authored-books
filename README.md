All of the works and books by [faraco](https://github.com/faraco) is licensed under [CC BY-ND 4.0](https://creativecommons.org/licenses/by-nd/4.0/).
